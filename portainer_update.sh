#!/bin/bash

# Determine the existing Portainer container's image
existing_image=$(docker ps --filter "name=portainer" --format "{{.Image}}")

# Check if the existing image contains "portainer-ce" or "portainer-ee" to determine the edition
if [[ "$existing_image" == *"portainer-ce"* ]]; then
  edition="ce"
  edition_display="Community"
elif [[ "$existing_image" == *"portainer-ee"* ]]; then
  edition="ee"
  edition_display="Enterprise"
else
  echo "Error: Unable to determine Portainer edition."
  exit 1
fi

# Echo the detected edition
echo "Detected Portainer Edition: $edition_display"

# Stop and remove the existing Portainer container
docker stop portainer
docker rm portainer

# Pull the latest Portainer image based on the detected edition
if [ "$edition" = "ee" ]; then
  docker pull portainer/portainer-ee:latest
else
  docker pull portainer/portainer-ce:latest
fi

# Run the updated Portainer container
docker run -d -p 8000:8000 -p 9443:9443 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-"$edition":latest

echo "Portainer $edition_display edition updated successfully."
