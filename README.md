# Portainer Updater

**An Effortless Way to Keep Portainer Up-to-Date**

Do you find updating Portainer to be a hassle? If so, you're in luck! I've created this ultra-simple script to streamline the process for you. 

## Why I Made This

I'll be honest; I made this script because I'm lazy. But sometimes, laziness can lead to innovative solutions, right?

## Features

- **Automatic Detection**: The script intelligently detects whether you're using Portainer Community or Enterprise and updates it accordingly.

- **Fixed Port Mappings**: Please note that this script assumes default port mappings (9443 and 8000). If you use different ports, you'll need to make some modifications.

## Usage

To get started, clone this repository and execute the script. Here's a quick rundown:

1. Clone the repository to your system.
2. Run the script.
3. Sit back, relax, and let the magic happen!

## Disclaimer

**Use at Your Own Risk**: While I've done my best to make this script as user-friendly as possible, it's important to remember that software updates can sometimes be unpredictable. Use this script with caution and be prepared for any unexpected issues.

Happy updating!
